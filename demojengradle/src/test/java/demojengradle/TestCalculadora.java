package demojengradle;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class TestCalculadora {
	@Test
	public void testSumar() {
		System.out.println("Test sumar");
		double number1 = 12;
		double number2 = 14;
		double target = 26;
		
		assertEquals(target, Calculadora.sumar(number1, number2));
	}
	@Test
	public void testRestar() {
		System.out.println("Test restar");
		double number1 = 14;
		double number2 = 12;
		double target = 2;
		
		assertEquals(target, Calculadora.restar(number1, number2));
	}
	@Test
	public void testMultiplicar() {
		System.out.println("Test multiplicar");
		double number1 = 12;
		double number2 = 14;
		double target = 168;
		
		assertEquals(target, Calculadora.multiplicar(number1, number2));
	}
	@Test
	public void testDividir() {
		System.out.println("Test dividir");
		double number1 = 12;
		double number2 = 12;
		double target = 1;
		
		assertEquals(target, Calculadora.dividir(number1, number2));
	}
}
